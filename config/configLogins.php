<?php
// - configLogin.php - //
$gestLogins=new gestLogins();
// - liste des utilisateurs - //
// -- les domaines qui ne sont pas explicitement autorisés sont interdit -- //

$u='user1';
$gestLogins->addUser($u,'user1-pwd');
$gestLogins->setDomaine('normal',1,$u);// autoriser l'accee au domain normal
$gestLogins->setDomaine('admin',0,$u); // (optionnel) surcharge la desactivation du domaine

$u='admin';
$gestLogins->addUser($u,'admin-pwd');
$gestLogins->setDomaine('admin',1,$u); // autoriser l'accee au domains 'admin'.
// ATTENTION Quand le domain 'admin' est activé l'user aura accée à tous les domaines quelle que soit leurs activations
$gestLogins->setDomaine('interdit',0,$u); // Cet user pourra quand même y accédé


?>
