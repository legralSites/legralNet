<!-- footer -->
<!-- footer gauche-->
<div id="footer">
<div id="footerGauche">
<?php
if(( defined('VERSIONSTATIQUE')) AND (VERSIONSTATIQUE == 1) ){
	echo 'version statique(g&eacute;n&eacute;r&eacute;e le: '.date('d/m/Y').')';
}
else{
	$d=microtime(TRUE)-SCRIPTDEBUT;
	echo 'page g&eacute;n&eacute;r&eacute;r en: '.$d.'s';
}
?></div><!-- footer gauche: fin-->

<!-- footer centre -->
<?php echo $gestMenus->getLastTitre().'<br>';   //afficher le titre de la page?>
<span class="licence">
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Licence Creative Commons" src="./styles/img/licenceCCBY-88x31.png" /></a><br />Mise &agrave; disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">licence Creative Commons Attribution 4.0 International</a>.<br />
<a xmlns:dct="http://purl.org/dc/terms/" href="http://legral.fr" rel="dct:source">http://legral.fr</a>.</span>
<!-- footer centre: fin -->

<!-- footer droit -->
<div id="footerDroit"></div>
<!-- footer droit: fin -->

</div><!-- footer -->
<?php include './piwik.php';?>
