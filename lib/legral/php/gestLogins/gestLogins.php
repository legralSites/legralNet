<?php
$gestLib->loadLib('gestLogins',__FILE__,'0.0.2-dev',"gestionnaire de connexion");
$gestLib->libs['gestLogins']->git='https://git.framasoft.org/legraLibs/gestLogins';
//	$gestLib->libs['gestLogins']->setErr(LEGRALERR::DEBUG);

//ini_set('display_errors',1);ini_set('error_reporting',E_ALL);
/*!******************************************************************
fichier: login.php
auteur : Pascal TOLEDO
date :15 octobre 2012
date de modification: 2015.04.20
source: http://www.legral.fr/intersites/lib/legral/php/gestLogins
depend de:
	* _session
description:
	* gestion des utilisateurs avec leur mot de passe
documentation:
*******************************************************************/
class gestLogin{
	public 	$login=NULL;public $pwd=NULL; // public for test only
	// domaine de connection: 0:connection au domaine interdite;1:autorisé;  (2:effective;3:via apache) -> ???
	public $attrs;
	public $domaines;

	function __construct($login,$pwd){
		$this->login=$login;
		$this->pwd=$pwd;
		$this->domaines=array();
		//$this->domaines['admin']=0;
		$this->attrs=new gestAttributs();
		//$this->gestDB=new legralPDO('test');
		$this->domaines['admin']=0;
	}

	function verifyPwd($pwd){return($pwd===$this->pwd)?1:0;}
	function setDomaine($domaine,$value=1){$this->domaines[$domaine]=$value;}


	// - l'user est il autorise dans ce domaine? - //
	function isDomaine($domaine){
		if($this->domaines['admin']===1)return 1;
	return(isset($this->domaines[$domaine])?$this->domaines[$domaine]===1:0);
		}

	// - afficher, sous forme de liste, tous les domaines de l'user - //
	function showDomaines(){
		$out='<ul>';
		foreach($this->domaines as $domaineKey=>$domaineValue)
			{$out.= "<li>$domaineKey : $domaineValue</li>";}
		$out.='</ul>';
		return "\n$out\n";
	}
	// - afficher, sous forme de liste, tous les domaines de l'user - //
	function showAttributs(){
		$out='<ul>';
		foreach($this->attrs->get() as $nom =>$val)
			{$out.= "<li>$nom : $val</li>";}
		$out.='</ul>';
		return "\n$out\n";
	}
}//class gestLogin

//
// - gestion des utilisateurs - //
//
class gestLogins{
	public $users=array();
//	private $au='';	// login de l'user actuellement connecte (laisser '' pour la comparaison avec NULL)
	public $au='';	// public lors des test
	function __construct(){
		global $gestLib;
		if(!empty($_SESSION['login']))$this->reconnect();
	}

	function whoIsConnected(){return $this->au;}

	function addUser($login,$pwd){
		if(isset($this->users[$login])){return -1;}
		else{$this->users[$login]=new gestLogin($login,$pwd);}
	}

	// - renvoie 1 si un user (ou l'AU) est connecte - //
	function isConnect($userId=NULL){
		global $gestLib;
		if($userId===NULL)$userId=$this->au;
		if(empty($userId))return 0;	// $u est empty bizar!
		return ($this->au===$userId)?1:0;
	}


	// - gestion des domaines d'autorisation - //

	function setDomaine($domaine,$val=1,$userId=NULL){
		if($userId===NULL)$userId=$this->au;
		if(isset($this->users[$userId]))return($this->users[$userId]->setDomaine($domaine,$val));
	}
	
	function isDomaine ($domaine,$userId=NULL){
		if($userId===NULL)$userId=$this->au;
		if(isset($this->users[$userId])){
			return($this->users[$userId]->isDomaine($domaine));
		}
	}
	
	function showDomaines ($userId=NULL){
		if($userId===NULL)$userId=$this->au;
		if(isset($this->users[$userId]))return $this->users[$userId]->showDomaines();
	}

	// - function pour l'ActualUser(AU) - //
	function AUsetDomaine($domaine,$value=0){if(isset($this->users[$this->au]))$this->users[$this->au]->setDomaine($domaine,$value);}



	// - gestion des domaines des attributs - //
	function setAttr($userId,$key,$val,$force=1){
		if(isset($this->users[$userId])){$this->users[$userId]->attrs->set($key,$val,$force);}
	}
	function _unsetAttr($userId,$key){
		if(isset($this->users[$userId])){$this->users[$userId]->attrs->_unset($key);}
	}

	function getAttr($userId,$key=NULL){
		if(isset($this->users[$userId])){return $this->users[$userId]->attrs->get($key);}
	}

	function showAttributs ($userId=NULL){
		if($userId===NULL)$userId=$this->au;
		if(isset($this->users[$userId]))return $this->users[$userId]->showAttributs();
	}



	// - gestion de la connexion/deco - //
	// - connecter un user avec son login pwd - //
	function connect($login,$pwd){
		global $gestLib;
		//echo  $gestLib->libs['gestLogins']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,'debut');
		if((isset($this->users[$login]))and($this->users[$login]->verifyPwd($pwd))){
			//echo  $gestLib->libs['gestLogins']->debugShow('gestLogins',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__METHOD__.':'.__LINE__,'Passwd Verifié!');
			$_SESSION['login']=$login;
			$_SESSION['isConnect']=1;
			$this->au=$login;return 1;
			//echo  $gestLib->libs['gestLogins']->debugShow('gestLogins',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__METHOD__.':'.__LINE__,'');

		}
		//echo  $gestLib->libs['gestLogins']->debugShowVar('gestLogins',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__METHOD__.':'.__LINE__,'$_SESSION["login"]', $_SESSION['login']);
		//echo  $gestLib->libs['gestLogins']->debugShowVar('gestLogins',LEGRALERR::DEBUG,NULL,__CLASS__.':'.__METHOD__.':'.__LINE__,'$this->au', $this->au);
		return 0;
	}

	// - reconnecter un user: recup la session pour une connexion auto - //
	function reconnect(){
		global $gestLib;
		//echo  $gestLib->libs['gestLogins']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,'tentative de reconnection');
		if(isset($_SESSION['login'])){
			//echo  $gestLib->libs['gestLogins']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,'reconnection ok');
			$this->au=$_SESSION['login'];
			return 1;
		}
		//echo  $gestLib->libs['gestLogins']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,'la reconnection à échoué');
		return 0;
	}

	function deconnect(){
		global $gestLib;
		global $_SESSION;
		echo  $gestLib->libs['gestLogins']->debugShow(LEGRALERR::DEBUG,__LINE__,__METHOD__,'debut');
		if($this->au!=='')
			{
			$_SESSION['login']='';$this->au='';return 1;
			//session_destroy();
			}
		return 0;
	}



	function tableau(){
		$out ='<table class=""><caption>gestLogins</caption>';
		$out.='<thead><tr><th>login</th><th>pwd</th><th>domaines</th><th>attributs</th></tr></thead>';
		foreach($this->users as $key => $user){
			$out.='<tr>';
			$out.='<td>'.$user->login.'</td>';
			$out.='<td>'.$user->pwd.'</td>';

			$out.='<td>';
			foreach($user->domaines as $nom =>$val){
				$out.="$nom : $val<br>";
			}	
			$out.='</td>';
			$out.='<td>';
			foreach($user->attrs->get() as $nom =>$val){
				$out.="$nom : $val<br>";
			}	
			$out.='</td>';

			$out.="</tr>\n";
			};

		$out.='</table>';
		return $out;	
	}
	
	
	
	// - exemple d'autologin - //
/*
	function autolog($test=0)
		{
		if($test)
			{
			if($_SERVER['SERVER_ADDR']==='127.0.0.1')
				{$login='pascal';
				$_SESSION['login']=$login;
				$this->connectSansPwd($login);
				}
			}

		if(($this->whoIsConnected()===NULL)and(isset($_SESSION['login'])))
			{
			$login=$_SESSION['login'];
			$this->connectSansPwd($login);
			}

		if(($this->whoIsConnected()===NULL)and(isset($_POST['login']))and(isset($_POST['pwd'])))
			{
			$login=$_POST['login'];$pwd=$_POST['pwd'];
			$this->connect($login,$pwd);
			}
		}
 */
} // class gestLogins


////////////////////////////////////////////////////
// exemple d'utilisation
//
// creation d'une instance
//$legralUsers=new legralUsers();
//
// ajout d'un utilisateur
// 
//$legralUsers->addUser('pascal','mot_de_passe');
//
// autoriser un domaine a un user
//$legralUsers->setDomaine('pascal','site');
// interdire un domaine a un user
//$legralUsers->setDomaine('pascal','site',0);


//
// test de l'autorisation d'un domaine pour un user
// if($legralUsers->isDomaine('pascal','site')){echo "l'utilisateur pascal est autoriser sur le domaine 'site' ".'<br>';}else{echo "l'utilisateur pascal N'est PAS autoriser sur le domaine 'site' ".'<br>';}
// connecter un user avec son login/pwd
//if($legralUsers->connect('pascal','test0')){}
// voir qui est connecter
//echo 'Qui est connecter:'.$legralUsers->whoIsConnected().'<br>';


$gestLib->setEtat('gestLogins',LEGRAL_LIBETAT::LOADED);
$gestLib->end('gestLogins');
?>
