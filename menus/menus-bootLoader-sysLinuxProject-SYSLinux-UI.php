<?php
// ==== menu: SYSLinux: menu graphique   ==== //

$mn='SYSLinux-UI';
$pagePath=PAGES_ROOT."legralNet/bootLoader/sysLinuxProject/SYSLinux/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre',"l'interface graphique");
        $m->setAttr($p,'menuTitre',"l'interface graphique");

$p='UI-background';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - UI - image de fond');
        $m->setAttr($p,'menuTitre','splash');

$p='UI-color';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - UI - les couleurs');
        $m->setAttr($p,'menuTitre','les couleurs');

$p='UI-positions';
	$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','SYSLinux - UI - positionner les &eacute;l&eacute;ments');
        $m->setAttr($p,'menuTitre','positionner les &eacute;l&eacute;ments');


?>
