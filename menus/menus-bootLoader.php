<?php
// ==== menu: bootLoader  ==== //

$mn='bootLoader';
$pagePath=PAGES_ROOT."legralNet/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','bootLoader');
        $m->setAttr($p,'titre',"les bootLoaders (grub,SysLinux)");
	
$p='grub';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre','grub');
        $m->setAttr($p,'titre',"logiciel d'amorcage Grub");
        $m->addCssA($p,'dossier1');


$p='sysLinuxProject';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre','sysLinuxProject');
        $m->setAttr($p,'titre',"logiciel d'amorcage sur disque et via le LAN:SYSLinuxProject");
        $m->addCssA($p,'dossier1');

$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'menuTitre',"$p");
        $m->setAttr($p,'titre',"$mn: $p");
	
	
// - inclusion des sous menus - //	
include('./menus/menus-bootLoader-grub.php');
include('./menus/menus-bootLoader-sysLinuxProject.php');

?>
