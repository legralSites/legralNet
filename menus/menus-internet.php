<?php
// ==== menu: internet  ==== //

$mn="internet";
$pagePath=PAGES_ROOT."legralNet/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','internet');
        $m->setAttr($p,'titre',"internet");
	
$p='ports';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','les ports');
        $m->setAttr($p,'titre',"les ports de communications");
//        $m->addCssA($p,'dossier1');

$p='internet-services';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre','les services');
        $m->setAttr($p,'titre',"les services d'internet");
        $m->addCssA($p,'dossier1');

	
// ==== menu: internet:services  ==== //
$mn='internet-services';
$pagePath=PAGES_ROOT."legralNet/internet/$mn/";


$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','les services');
        $m->setAttr($p,'titre',"les services d'internet");


$p='mails';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','les mails');
        $m->setAttr($p,'titre',"les mails");


$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'menuTitre',"annexes");
        $m->setAttr($p,'titre',"$mn: annexes");

// ==== menu: internet:services:mail  ==== //
$mn='mails';
$pagePath=PAGES_ROOT."legralNet/internet/internet-services/$mn/";


$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','les mails');
        $m->setAttr($p,'titre',"les mails");

$p='mails-chiffrer';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'menuTitre',"chiffrer");
        $m->setAttr($p,'titre',"chiffrer ses mails");

?>
