<?php
// ==== menu: langages  ==== //
$mn='langages';
$pagePath=PAGES_ROOT."legralNet/$mn/";

$p='accueil';	// cette page est une page
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','langages');
        $m->setAttr($p,'menuTitre','langages');

$p='langages-shell';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'titre','langage Shell');
        $m->setAttr($p,'menuTitre','shell');
	$m->addCssA($p,'dossier1');

$p='langages-html';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'titre','langage HTML');
        $m->setAttr($p,'menuTitre','HTML');
	$m->addCssA($p,'dossier1');

$p='langages-css';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'titre','langage CSS');
        $m->setAttr($p,'menuTitre','CSS');
        $m->addCssA($p,'dossier1');

$p='langages-php';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'titre','langage PHP');
        $m->setAttr($p,'menuTitre','PHP');
        $m->addCssA($p,'dossier1');

$p='langagespython';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'titre','langage PYTHON');
        $m->setAttr($p,'menuTitre','PYTHON');
        $m->addCssA($p,'dossier1');

// ==== menu: shell  ==== //
$mn='langages-html';
$pagePath=PAGES_ROOT."legralNet/langages/$mn/";

$p='accueil';	// cette page est une page
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','langage Shell');
        $m->setAttr($p,'menuTitre','shell');

$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','shell -annexes');
        $m->setAttr($p,'menuTitre','annexes');


// ==== menu: html  ==== //
$mn='langages-html';
$pagePath=PAGES_ROOT."legralNet/langages/$mn/";

$p='accueil';	// cette page est une page
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','langage HTML');
        $m->setAttr($p,'menuTitre','HTML');

$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','HTML -annexes');
        $m->setAttr($p,'menuTitre','annexes');

// ==== menu: css  ==== //
$mn='langages-css';
$pagePath=PAGES_ROOT."legralNet/langages/$mn/";

$p='accueil';	// cette page est une page
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','langage CSS');
        $m->setAttr($p,'menuTitre','CSS');

$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','HTML -annexes');
        $m->setAttr($p,'menuTitre','annexes');


// ==== menu: php  ==== //
$mn='langages-php';
$pagePath=PAGES_ROOT."legralNet/langages/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','langage PHP');
        $m->setAttr($p,'menuTitre','PHP');

$p='divers';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','PHP - divers');
        $m->setAttr($p,'menuTitre','divers');
	
$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','Python -annexes');
        $m->setAttr($p,'menuTitre','annexes');

	

// ==== menu: python  ==== //
$mn='langages-python';
$pagePath=PAGES_ROOT."legralNet/langages/$mn/";

$p='accueil';	// cette page est une page
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','langage Python');
        $m->setAttr($p,'menuTitre','Python');

$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','Python -annexes');
        $m->setAttr($p,'menuTitre','annexes');


// ==== menu: javascript  ==== //
$mn='langages-javascript';
$pagePath=PAGES_ROOT."legralNet/langages/$mn/";

$p='accueil';	// cette page est une page
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'titre','langage JavaScript');
$m->setAttr($p,'menuTitre','JavaScript');

$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'titre','JavaScript -annexes');
        $m->setAttr($p,'menuTitre','annexes');


?>
