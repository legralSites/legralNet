<?php
// ==== menu: GNU/Linux:distributions:Debian:live ==== //

$mn='debian-live';
$pagePath=PAGES_ROOT.'legralNet/linux-distributions/debian/debian-live/';

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
	$m->setAttr($p,'titre','debianLive - accueil');
	$m->setAttr($p,'menuTitre','Debian Live');

$p='debian-live-imagePerso';
$m->addCallPage($p,$pagePath.'image-perso-accueil.html');

	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image");
	$m->setAttr($p,'menuTitre',"live-build:Cr&eacute;ation d'une image");
	$m->addCssA($p,'dossier1');

$p='image-perso-live-boot';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"Live-boot:personalisation du chargement d'une image Debian");
	$m->setAttr($p,'menuTitre','live-boot');

$p='image-copier';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre','copier une image sur le support (CD/USB)');
	$m->setAttr($p,'menuTitre','copier une image sur le support');

$p='debianLiveImage-persistence';
$m->addCallPage($p,$pagePath.'persistence.htm');
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre','persistence');
	$m->setAttr($p,'menuTitre','persistence');

$p='SYSLinux-annexes';
$m->addCallPage($p,PAGES_ROOT."legralNet/bootLoader/sysLinuxProject/SYSLinux/$p/accueil.html");
        $m->setAttr($p,'titre','annexes');
        $m->setAttr($p,'menuTitre','annexes');
        $m->addCssA($p,'dossier1');

// ==== menu: debianLiveImagePerso  ==== //
$mn='debian-live-imagePerso';
$pagePath=PAGES_ROOT.'legralNet/linux-distributions/debian/debian-live/';

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath.'image-perso-accueil.php');
	$m->setAttr($p,'titre','debianLive - accueil');
	$m->setAttr($p,'menuTitre','Debian Live Image Perso');

$p='image-perso-preparation';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image Debian: pr&eacute;paration");
	$m->setAttr($p,'menuTitre','pr&eacute;paration');

$p='image-perso-proxy';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image Debian: utilisation de proxy");
	$m->setAttr($p,'menuTitre','Utilisation de proxy');

$p='image-perso';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image Debian: les bases");
	$m->setAttr($p,'menuTitre','les bases');
	
$p='image-perso-packtages-list';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image Debian:ajout d'un logiciel via les packtages-list");
	$m->setAttr($p,'menuTitre','packtages-list');

$p="image-perso-ajout_deb";
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image Debian:ajout d'un .deb");
	$m->setAttr($p,'menuTitre',"rajout d'un .deb");

$p='image-perso-includes';
$m->addCallPage($p,$pagePath."$p.html");

	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image Debian:ajout de fichiers quelconques");
	$m->setAttr($p,'menuTitre','ajout de fichiers quelconques');

$p='image-perso-hooks';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image Debian: execution de script(les hooks)");
	$m->setAttr($p,'menuTitre','execution de script');

$p='image-perso-references';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre',"Cr&eacute;ation d'une image Debian: r&eacute;f&eacute;rences");
	$m->setAttr($p,'menuTitre','R&eacute;f&eacute;rences');
?>
