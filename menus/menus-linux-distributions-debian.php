<?php
// ==== menu: GNU/Linux:distributions:Debian ==== //

$mn='linux-distributions-debian';
$pagePath=PAGES_ROOT.'legralNet/linux-distributions/debian/';

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
	$m->setAttr($p,'menuTitre','GNU/Linux Debian');
	$m->setAttr($p,'titre','accueil(bas de page)');
//	$m->setMeta($p,'title','tutoriels - accueil(meta)');


$p='debian-install';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre','installation');
        $m->setAttr($p,'titre','Installation');
        $m->addCssA($p,'dossier1');

$p='debian-live';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre','live');
        $m->setAttr($p,'titre','créer une Debian GNU/linux Debian Live');
        $m->addCssA($p,'dossier1');


$p='debian-annexes';
$m->addCallPage($p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','annexes');
        $m->setAttr($p,'titre','GNU/Linux Debian annexes');


// ==== menu: debianInstall ==== //
	
$mn='debian-install';
$pagePath=PAGES_ROOT.'legralNet/linux-distributions/debian/debian-install/';
	
$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
	$m->setAttr($p,'titre','GNU/linux Debian: installation');
	$m->setAttr($p,'menuTitre','installation');

$p='install-minimal';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'titre','GNU/linux Debian: installation minimale');
	$m->setAttr($p,'menuTitre','installation minimal');


include('./menus/menus-linux-distributions-debian-live.php');


?>
