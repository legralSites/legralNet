<?php
// ==== menu: logiciels  ==== //

$mn='logiciels';
$pagePath=PAGES_ROOT."legralNet/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        $m->setAttr($p,'menuTitre','logiciels');
        $m->setAttr($p,'titre',"les logiciels");
	
$p='openelec';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr($p,'menuTitre','OpenElec');
        $m->setAttr($p,'titre',"openElec:mediaCenter");
        $m->addCssA($p,'dossier1');
	
$p='filezilla';
$m->addCallPage($p,$pagePath.$p."/$p.html");
        $m->setAttr($p,'menuTitre','FileZilla');
        $m->setAttr($p,'titre',"un client ftp: FileZilla");
//        $m->addCssA($p,'dossier1');


$p='annexes';
$m->addCallPage($p,$pagePath."$p.html");
	$m->setAttr($p,'menuTitre',"$p");
        $m->setAttr($p,'titre',"$mn: $p");
	
	
// - inclusion des sous menus - //	
include('./menus/menus-logiciels-openelec.php');

?>
